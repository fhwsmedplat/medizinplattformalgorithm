﻿package com.gorou21.algorithm.logic;

import com.gorou21.algorithm.types.Disease;

import java.util.ArrayList;

public class TestData {

	/*
	Class storing example data for symptoms as well as diseases.
	Has to suffice until fetching symptoms and diseases from DB works.
	 */

	public static String[] getDiseases()
    {
        return new String[]
                {
                        "Lebensmittelvergiftung",
                        "Grippe",
                        "Erkältung",
                        "Migräne",
                        "Gicht",
                        "Blindarmentzündung",
                        "Mandelentzündung",
                        "Heuschnupfen",
                        "Lungenentzündung",
                        "Mittelohrentzündung"
                };
    }

    // Returns a Disease object via name for easier construction.
    public static Disease disease(String name)
    {
        switch (name)
        {
            case "Lebensmittelvergiftung":
                return new Disease(name, fetchSymptomsForDisease(name), "Aspirin");
            case "Grippe":
                return new Disease(name, fetchSymptomsForDisease(name), "Grippostad C Hartkapseln");
            case"Erkältung":
                return new Disease(name, fetchSymptomsForDisease(name), "Nasenspray Ratiopharm");
            case"Migräne":
                return new Disease(name, fetchSymptomsForDisease(name), "Paracetamol");
            case"Gicht":
                return new Disease(name, fetchSymptomsForDisease(name), "Antirheumatika");
            case"Blindarmentzündung":
                return new Disease(name, fetchSymptomsForDisease(name), "Penicilin");
            case"Mandelentzündung":
                return new Disease(name, fetchSymptomsForDisease(name), "Penicilin");
            case"Heuschnupfen":
                return new Disease(name, fetchSymptomsForDisease(name), "Kortison");
            case"Lungenentzündung":
                return new Disease(name, fetchSymptomsForDisease(name), "Penicilin");
            case"Mittelohrentzündung":
                return new Disease(name, fetchSymptomsForDisease(name), "Penicilin");

        }
        return null;
    }

    // Returns ArrayList<String> of symptoms to more easily construct a named disease.
    public static ArrayList<String> fetchSymptomsForDisease(String name)
    {
        ArrayList<String> symptoms = new ArrayList();
        switch(name){
            case "Lebensmittelvergiftung":
                symptoms.add("Bauchschmerzen");
                symptoms.add("Übelkeit");
                symptoms.add("Durchfall");
                return symptoms;

            case "Grippe":
                symptoms.add("Kopfschmerzen");
                symptoms.add("Fieber");
                symptoms.add("Schnupfen");
                symptoms.add("Übelkeit");
                return symptoms;

            case "Erkältung":
                symptoms.add("Schnupfen");
                symptoms.add("Kopfschmerzen");
                return symptoms;

            case "Migräne":
                symptoms.add("Kopfschmerzen");
                symptoms.add("Durchfall");
                symptoms.add("Lichtempfindlichkeit");
                return symptoms;

            case "Gicht":
                symptoms.add("Gelenkschmerzen");
                symptoms.add("Kopfschmerzen");
                symptoms.add("Übelkeit");
                return symptoms;

            case "Blindarmentzündung":
                symptoms.add("Bauchschmerzen");
                symptoms.add("Übelkeit");
                symptoms.add("Appetitlosigkeit");
                return symptoms;

            case "Mandelentzündung":
                symptoms.add("Halsschmerzen");
                symptoms.add("Schwellung am Hals");
                return symptoms;

            case "Heuschnupfen":
                symptoms.add("Schnupfen");
                symptoms.add("Schwellung der Augen");
                return symptoms;

            case "Lungenentzündung":
                symptoms.add("Schwellung am Hals");
                symptoms.add("Kopfschmerzen");
                symptoms.add("Herzrasen");
                symptoms.add("Schüttelfrost");
                return symptoms;

            case "Mittelohrentzündung":
                symptoms.add("Ohrenschmerzen");
                symptoms.add("Fieber");
                symptoms.add("Kopfschmerzen");
                return symptoms;
        }
        return null;
    }
}
